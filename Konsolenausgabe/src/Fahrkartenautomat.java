﻿import java.text.DecimalFormat;
import java.util.*;

class Fahrkartenautomat {
	public static void main(String[] args) {

		while (true) {
			System.out.println("------------------------------------------------------\n");
			Scanner tastatur = new Scanner(System.in);
			double gesamtBetrag = fahrkartenbestellungErfassen(tastatur);
			double rueckgeldbetrag = fahrkartenBezahlen(gesamtBetrag, tastatur);
			FahrscheinAusgabe();
			RueckgeldAusgabe(rueckgeldbetrag);
		}
	}

	// Die Tastatur wird als Parameter übergeben, damit kein neuer Scanner erzeugt
	// werden muss
	public static double fahrkartenbestellungErfassen(Scanner tastatur) {
		final double PREIS_EINZELFAHRSCHEIN = 2.9;
		final double PREIS_TAGESKARTE = 8.6;
		final double PREIS_KLEINGRUPPE = 23.5;
		int auswahlFahrkartenart;
		boolean firstRun = true;

		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
		System.out.printf("Einzelfahrschein Regeltarif AB [%.2f EUR] (1)\n", PREIS_EINZELFAHRSCHEIN);
		System.out.printf("Tageskarte  Regeltarif AB [%.2f EUR] (2)\n", PREIS_TAGESKARTE);
		System.out.printf("Kleingruppen-Tageskarte Regeltarif AB [%.2f EUR] (3)\n", PREIS_KLEINGRUPPE);

		do {
			if (!firstRun) {
				System.out.println("Ungültige Eingabe! Ihre Wahl muss zwischen 1 und 3 liegen.");
			}
			System.out.print("Ihre Wahl: ");
			auswahlFahrkartenart = tastatur.nextInt();
			firstRun = false;
		} while (!((auswahlFahrkartenart > 0) && (auswahlFahrkartenart < 4)));

		double zuZahlenderBetrag;
		switch (auswahlFahrkartenart) {
		case 1:
			zuZahlenderBetrag = PREIS_EINZELFAHRSCHEIN;
			break;
		case 2:
			zuZahlenderBetrag = PREIS_TAGESKARTE;
			break;
		case 3:
			zuZahlenderBetrag = PREIS_KLEINGRUPPE;
			break;
		default:
			zuZahlenderBetrag = 0;
		}
		
		firstRun = true;
		
		int ticketAnzahl;
		do {
			if (!firstRun) {
				System.out.println("Ungültige Eingabe, bitte eine Zahl zwischen 1 und 10 wählen!");
			}
			System.out.print("Ticket Anzahl : ");
			ticketAnzahl = tastatur.nextInt();
			firstRun = false;
		} while ((ticketAnzahl > 10) || (ticketAnzahl < 1));

		double ergebnis = zuZahlenderBetrag * ticketAnzahl;

		return ergebnis;

	}

	// Die Tastatur wird als Parameter übergeben, damit kein neuer Scanner erzeugt
	// werden muss
	public static double fahrkartenBezahlen(double zuZahlen, Scanner tastatur) {

		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", zuZahlen - eingezahlterGesamtbetrag);
			System.out.print("Eingabe (mind. 5Ct, höchstens 100 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
		return rückgabebetrag;
	}

	public static void FahrscheinAusgabe() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	public static void RueckgeldAusgabe(double rückgabebetrag) {

		if (rückgabebetrag > 0.0) {

			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO", rückgabebetrag);
			System.out.println("wird in folgenden Münzen und Scheinen ausgezahlt:");

			while (rückgabebetrag >= 100.0) // 100 EURO-Schein
			{
				System.out.println("100EURO");
				rückgabebetrag -= 100.0;
			}

			while (rückgabebetrag >= 50.0) // 50 EURO-Schein
			{
				System.out.println("50EURO");
				rückgabebetrag -= 50.0;
			}

			while (rückgabebetrag >= 20.0) // 20 EURO-Schein
			{
				System.out.println("20 EURO");
				rückgabebetrag -= 20.0;
			}

			while (rückgabebetrag >= 10.0) // 10 EURO-Schein
			{
				System.out.println("10 EURO");
				rückgabebetrag -= 10.0;
			}

			while (rückgabebetrag >= 5.0) // 5 EURO-Schein
			{
				System.out.println("5 EURO");
				rückgabebetrag -= 5.0;
			}
			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
